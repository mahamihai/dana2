﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 4/9/2018 10:04:08 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace DAL.Models
{

    /// <summary>
    /// There are no comments for DAL.Models.Submission in the schema.
    /// </summary>
    public partial class Submission {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for Submission constructor in the schema.
        /// </summary>
        public Submission()
        {
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for AssignmentId in the schema.
        /// </summary>
        public virtual int AssignmentId
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Git in the schema.
        /// </summary>
        public virtual string Git
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ShortRemark in the schema.
        /// </summary>
        public virtual string ShortRemark
        {
            get;
            set;
        }
    }

}
