USE [master]
GO
/****** Object:  Database [Stocks]    Script Date: 4/14/2018 2:05:27 PM ******/
CREATE DATABASE [Stocks]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Stocks', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Stocks.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Stocks_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Stocks_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Stocks] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Stocks].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Stocks] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Stocks] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Stocks] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Stocks] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Stocks] SET ARITHABORT OFF 
GO
ALTER DATABASE [Stocks] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Stocks] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Stocks] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Stocks] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Stocks] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Stocks] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Stocks] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Stocks] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Stocks] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Stocks] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Stocks] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Stocks] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Stocks] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Stocks] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Stocks] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Stocks] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Stocks] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Stocks] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Stocks] SET  MULTI_USER 
GO
ALTER DATABASE [Stocks] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Stocks] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Stocks] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Stocks] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Stocks] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Stocks] SET QUERY_STORE = OFF
GO
USE [Stocks]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Stocks]
GO
/****** Object:  Table [dbo].[Records]    Script Date: 4/14/2018 2:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Records](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[price] [float] NOT NULL,
	[quantity] [int] NOT NULL,
	[date] [date] NOT NULL,
	[instrument] [int] NOT NULL,
 CONSTRAINT [PK_Records] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shares]    Script Date: 4/14/2018 2:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shares](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nchar](20) NULL,
	[price] [float] NULL,
 CONSTRAINT [PK_Shares] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 4/14/2018 2:05:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nchar](20) NOT NULL,
	[password] [nchar](20) NOT NULL,
	[shares] [float] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Records] ON 

INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (7, 4, 6, CAST(N'2018-04-13' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (9, 4, -3, CAST(N'2018-04-13' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (10, 4, -3, CAST(N'2018-04-13' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (11, 2, 3, CAST(N'2018-04-13' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (12, 4, 3, CAST(N'2018-04-13' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (13, 4, -2, CAST(N'2018-04-13' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (14, 2, -3, CAST(N'2018-04-13' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (15, 4, -1, CAST(N'2018-04-13' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (16, 4, 6, CAST(N'2018-04-13' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (17, 4, -6, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (18, 4, 6, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (19, 4, -6, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (20, 2, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (21, 2, -3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (22, 2, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (23, 4, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (24, 4, -3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (25, 4, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (26, 2, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (27, 2, -3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (28, 4, -3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (29, 4, -3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (30, 4, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (31, 4, -3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (32, 2, 3, CAST(N'2018-04-14' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (33, 4, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (34, 4, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (35, 4, -3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (36, 4, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (37, 4, -6, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (39, 2, -3, CAST(N'2018-04-14' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (40, 3, 4, CAST(N'2018-04-14' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (41, 2, -3, CAST(N'2018-04-14' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (42, 2, -1, CAST(N'2018-04-14' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (43, 3, 3, CAST(N'2018-04-14' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (45, 4, 5, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (46, 4, 5, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (47, 4, -5, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (48, 2, 2, CAST(N'2018-04-14' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (49, 4, 3, CAST(N'2018-04-14' AS Date), 2)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (50, 2, -3, CAST(N'2018-04-14' AS Date), 1)
INSERT [dbo].[Records] ([id], [price], [quantity], [date], [instrument]) VALUES (51, 4, 1, CAST(N'2018-04-14' AS Date), 2)
SET IDENTITY_INSERT [dbo].[Records] OFF
SET IDENTITY_INSERT [dbo].[Shares] ON 

INSERT [dbo].[Shares] ([id], [name], [price]) VALUES (1, N'Sony                ', 2)
INSERT [dbo].[Shares] ([id], [name], [price]) VALUES (2, N'Samsung             ', 4)
SET IDENTITY_INSERT [dbo].[Shares] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([id], [username], [password], [shares]) VALUES (1, N'maha                ', N'maha                ', 7)
SET IDENTITY_INSERT [dbo].[Users] OFF
USE [master]
GO
ALTER DATABASE [Stocks] SET  READ_WRITE 
GO
