﻿using System.Linq;
using System.Reflection;
using HM2_Dao;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;

namespace HM2.Dao
{
    public class Services
    {
        public Services()
        {

        
           

        }

        public ISession getConnection()
        {
            var cfg = new Configuration();



            cfg.DataBaseIntegration(x => {

                x.ConnectionString = " Data Source = localhost; Initial Catalog =Stocks; Integrated Security = True";




                x.Driver<SqlClientDriver>();
                x.Dialect<MsSql2008Dialect>();

            });

            cfg.AddAssembly(Assembly.GetExecutingAssembly());
            var t=Assembly.GetExecutingAssembly();
            cfg.AddAssembly("HM2.Models");
            var sefact = cfg.BuildSessionFactory();
            return sefact.OpenSession();

        }
        public User CheckUser(string username, string password)
        {
            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    //perform database logic 
                    var m = session.Query<User>().ToList();
                    var useru=session.Query<User>().Where(x => x.Password.Trim().Equals(  password) && x.Username.Trim().Equals( username)).FirstOrDefault();
                    return useru;
                }
            }

        }







    }


   
    
}
