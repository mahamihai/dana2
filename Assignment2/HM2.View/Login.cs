﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using HM2.Models;
using HM2.View.Interfaces;
using ILoginView = HM2.View.Interfaces.ILoginView;

namespace HM2.View
{
    public partial class Login : Form,ILoginView
    {
        public Login()
        {
            InitializeComponent();
           // this.Show();
        }
        private ILoginController _control;

        public void registerController(ILoginController controller)
        {
            this._control = controller;
        }
     

        public void loginBtn(Object sender, EventArgs e)
        {
            string username = this.textBox1.Text.Trim();
            string password = this.textBox2.Text.Trim();
            User user= this._control.checkUser(username, password);
          
        }

        public void showMessage(string message)
        {
            MessageBox.Show(message);
        }

        public void display()
        {
            //this.Visible = true;
            this.ShowDialog();

        }
    }
}
