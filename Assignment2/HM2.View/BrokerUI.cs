﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM2.Models;
using HM2.View.Interfaces;
using IBrokerView = HM2.View.Interfaces.IBrokerView;

namespace HM2.View
{
    public partial class BrokerUI : Form, IBrokerView
    {
        public BrokerUI()
        {
            InitializeComponent();
            disableAll();
        }

        private IBrokerController _controler;

        public void displayShares(List<Share> shares) //arata actiunile
        {
            this.dataGridView1.DataSource = shares;
        }

        public void registerController(IBrokerController controler) //salveaza controlerul viewului
        {
            this._controler = controler;

        }

        public void display() //porneste formul
        {
            //this.Visible = true;
            this.ShowDialog();

        }

        public void showStocksBut(Object sender, EventArgs e) //apeleaza controleru sa arate actiunile
        {

            disableAll();
            this.dataGridView1.Visible = true;
            this.panel1.Visible = true;
            this._controler.showShares();
        }

        public void updateMoney(double money) //da refresh la bani
        {
            this.moneyLabel.Text = money.ToString();
        }

        public void displayHistory(List<Record> records)
        {
            this.dataGridView2.DataSource = records;
        }

        private int selectedId;
        private Record selectedRecord;

        private void dataGridView1_CellContentClick1(object sender, DataGridViewCellEventArgs e)
        {
            int id;
            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["Id"].Value.ToString();
                id = int.Parse(value);
            }
            catch
            {
                return;
            }

            this.selectedId = id;
            this._controler.computeStock(id);
        }

        private void dataGridView1_CellContentClick2(object sender, DataGridViewCellEventArgs e)
        {
            int id;
            int price;
            double quantity;
            try
            {
                int row = e.RowIndex;
                this.selectedRecord = this.dataGridView2.Rows[row].DataBoundItem as Record;

                string value = this.dataGridView2.Rows[row].Cells["Id"].Value.ToString();
                id = int.Parse(value);
                value = this.dataGridView2.Rows[row].Cells["Price"].Value.ToString();
                price = int.Parse(value);
                value = this.dataGridView2.Rows[row].Cells["Quantity"].Value.ToString();
                quantity = double.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedId = id;
            this.quantBox.Text = quantity.ToString();
            this.priceBox.Text = price.ToString();

        }


        public void showMessage(string message)
        {
            MessageBox.Show(message);
        }

        public void displayStock(double stock)
        {
            this.stockBox.Text = stock.ToString();
        }

        public void buyBut(object sender, EventArgs e)
        {
            int quantity = 0;
            float price = 0;
            try
            {
                quantity = int.Parse(quantityBox.Text);
                string priceString = this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells["Price"]
                    .Value.ToString();
                price = int.Parse(priceString);

            }
            catch
            {
                showMessage("Converting:Not a valid input");
                return;
            }

            this._controler.transaction(selectedId, price, quantity);


        }

        public void sellBut(object sender, EventArgs e)
        {
            int quantity = 0;
            float price = 0;
            try
            {
                quantity = -int.Parse(quantityBox.Text);
                string priceString = this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells["Price"]
                    .Value.ToString();
                price = int.Parse(priceString);

            }
            catch
            {
                showMessage("Converting:Not a valid input");
                return;
            }

            this._controler.transaction(selectedId, price, quantity);


        }



        public void disableAll() //disable most of the UI objects except for buttons
        {
            this.dataGridView1.Visible = false;
            this.dataGridView2.Visible = false;
            this.panel1.Visible = false;
            this.panel2.Visible = false;
            this.panel3.Visible = false;

        }

        public void showHistory(object sender, EventArgs e)
        {
            disableAll();
            this.dataGridView2.Visible = true;
            this.dataGridView2.DataSource = null;
            this.panel2.Visible = true;
            this._controler.showHistory();

        }

        public void updateButton(object sender, EventArgs e)
        {
            double newPrice = 0;
            int newQuantity = 0;
            Record selected = this.selectedRecord;


            try
            {
                newPrice = double.Parse(this.priceBox.Text);
                newQuantity = int.Parse(this.quantBox.Text);
            }
            catch (Exception)
            {
                Console.WriteLine();
                throw;
            }

            this._controler.updateTransaction(selected, newPrice, newQuantity);
            this._controler.showHistory();
        }

        public void deleteBut(object sender, EventArgs e)
        {
            if (this.selectedRecord != null)
            {
                //make a new transaction that is oposed to the one you selected 
                this._controler.deleteRecord(selectedRecord.Price, selectedRecord.Quantity,this.selectedRecord);
            }

            this._controler.showHistory();
        }

        public void eodBut(object selnder, EventArgs e)
        {
            //disable all controls for buying selling
            this.disableAll();
            this.button1.Visible = false;
            this.button2.Visible = false;
            this.panel3.Visible = true; //enable report buttons

        }

        public void intervalButton(object selnder, EventArgs e)
        {
            DateTime start = this.dateTimePicker1.Value.Date;
            DateTime stop = this.dateTimePicker2.Value.Date;
            this._controler.generateIntervalReport(start, stop);
        }

        public void todayReportBut(object selnder, EventArgs e)
            {
                
                this._controler.generateTodayReport();

            }

        private void button10_Click(object sender, EventArgs e)
        {
            this._controler.switchToClassicDB();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this._controler.switchToHibernate();
        }
    }
}
