﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.Models;

namespace HM2.View.Interfaces
{
  public   interface IBrokerController
  {
      void showShares();
      void displayMoney();
      void showHistory();
      void computeStock(int id);

      void transaction(int id,double price, int quantity);
      void updateTransaction(Record transaction, double price, int quantity);
      void generateIntervalReport(DateTime start, DateTime stop);
      void generateTodayReport();
        void switchToClassicDB();
        void switchToHibernate();
      void deleteRecord(double price, int quantity,Record record);
  }
}
