﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.Models;

namespace HM2.View.Interfaces
{
   public interface ILoginController
   {
       User checkUser(string username, string password);
   }
}
