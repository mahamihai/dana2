﻿using System.Collections.Generic;
using System.Net;
using HM2.Models;

namespace HM2.View.Interfaces
{
   public interface IBrokerView
    {


        //interfata pentru viewul de broker
  
        void registerController(IBrokerController controller);
        void display();
        void  displayShares(List<Share> shares);
        void updateMoney(double money);
        void displayHistory(List<Record> record);
        void showMessage(string message);
        void displayStock(double stock);
    }
}
