﻿using System.Collections.Generic;
using System.Linq;
using HM2.Models.DB;
namespace HM2.Models
{
    public class ClassicDao:IDao
    {
       
        public User checkUser(string username, string password)
        {
            UserDao userDao = new UserDao();
            var users = userDao.GetAll("dbo.Users");
            var theOne = users.Find(x => x.Username.Equals(username) && x.Password.Equals(password));
            return theOne;
        }

        public void deleteRecord(Record record)
        {
            var all = this.getRecords();
            var t = all.Where(
                p => p.Quantity == record.Quantity && p.Price == record.Price &&
                     p.Instrument == record.Instrument).LastOrDefault();
            RecordDao recordDao=new RecordDao();
            recordDao.DeleteById(t.Id,"dbo.Records");
        }

        public List<Share> getShares()
        {
            ShareDao shareDao = new ShareDao();
            return shareDao.GetAll("dbo.Shares");
        }
        public List<Record> getRecords()
        {
            RecordDao recordDao = new RecordDao();
            return recordDao.GetAll("dbo.Records");
        }
      
    
     
       public void updateUser(User user)
        {
            UserDao userDao = new UserDao();
            userDao.UpdateBy(new string[] { "id" }, new string[] { user.Id.ToString() }, user,"dbo.Users");

        }
        public void addRecord(Record record)
        {
            RecordDao recordDao = new RecordDao();
            recordDao.Insert(record, "dbo.Records");

        }
        public void updateRecord(Record record)
        {
            RecordDao recordDao = new RecordDao();
            recordDao.UpdateBy(new string[] { "id" },new string[] { record.Id.ToString() },record, "dbo.Records");
        }
    }
}
