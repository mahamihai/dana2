﻿using System.Collections.Generic;

namespace HM2.Models
{
  public  interface IDao
  {
      User checkUser(string username, string password);//verifica useru;
      List<Share> getShares();//returneaza toate actiunile
      List<Record> getRecords();
      void updateUser(User user);
      void addRecord(Record record);
      void updateRecord(Record record);
      void deleteRecord(Record record);
  }
}
