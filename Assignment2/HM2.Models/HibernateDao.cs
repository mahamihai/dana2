﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using Remotion.Linq.Clauses.ResultOperators;

namespace HM2.Models
{
    public class HibernateDao : IDao
    {
        public void deleteRecord(Record record)
        {
            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    var all = this.getRecords();
                    var t =all.Where(
                        p => p.Quantity == record.Quantity && p.Price == record.Price &&
                                    p.Instrument == record.Instrument ).LastOrDefault();

                    session.Delete(t);
                    tx.Commit();
                    
                }
            }
        }

        public ISession getConnection()
        {
            var cfg = new Configuration();



            cfg.DataBaseIntegration(x =>
            {

                x.ConnectionString = " Data Source = localhost; Initial Catalog =Stocks; Integrated Security = True";




                x.Driver<SqlClientDriver>();
                x.Dialect<MsSql2008Dialect>();

            });

            cfg.AddAssembly(Assembly.GetExecutingAssembly());
            var t = Assembly.GetExecutingAssembly();
            var sefact = cfg.BuildSessionFactory();
            return sefact.OpenSession();

        }

        public User checkUser(string username, string password)
        {
            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    //perform database logic 
                    var useru = session.Query<User>()
                        .Where(x => x.Password.Trim().Equals(password) && x.Username.Trim().Equals(username))
                        .FirstOrDefault();

                    return useru;
                }
            }

        }

        public List<Share> getShares()
        {
            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    return session.Query<Share>().ToList();
                }
            }
        }
        public List<Record> getRecords()
        {
            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    return session.Query<Record>().ToList();
                }
            }
        }

        public void updateUser(User user)
        {

            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(user);
                    tx.Commit();
                }
            }
        }
        public void addRecord(Record record)
        {

            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    session.Save(record);
                    tx.Commit();
                }
            }
        }

        public void updateRecord(Record record)
        {
            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    session.Update(record);
                    var t = session.Query<Record>();
                    tx.Commit();
                }
            }
        }
    }
}
