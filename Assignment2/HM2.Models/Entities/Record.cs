﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 4/13/2018 8:01:37 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace HM2.Models
{

    /// <summary>
    /// There are no comments for HM2.Models.Record, HM2.Models in the schema.
    /// </summary>
    public partial class Record {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for Record constructor in the schema.
        /// </summary>
        public Record()
        {
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Price in the schema.
        /// </summary>
        public virtual double Price
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Quantity in the schema.
        /// </summary>
        public virtual int Quantity
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Date in the schema.
        /// </summary>
        public virtual System.DateTime Date
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Instrument in the schema.
        /// </summary>
        public virtual int Instrument
        {
            get;
            set;
        }
    }

}
