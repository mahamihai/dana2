﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using HM2.Models;
using HM2.View.Interfaces;
using System.Globalization;

namespace HM2.Controller
{
    public class BrokerController:IBrokerController
    {
        private IDao _dao;
        private IBrokerView _view;
        private User user;
        public BrokerController(IBrokerView view,IDao dao,User user)//seteaza viewul controlerului si useru care s-a logat
        {
            this._dao = dao;//register fields
            this._view = view;
            this.user = user;
            this._view.registerController(this);
            displayMoney();




            this._view.display();
          
        }

        private List<Record> dailyRecords=new List<Record>();

        public void showShares()//ia actiunile din db si trimitele la ui sa le arate
        {
            var stocks=this._dao.getShares();
            this._view.displayShares(stocks);

        }

        public void displayMoney()//updateaza cati bani are clientul
        {
            this._view.updateMoney(this.user.Shares);
        }

        public void showHistory()
        {
           
            this._view.displayHistory(dailyRecords);

        }

        private int computeAvailableStock(int id, List<Record> history)
        {
            int current = 0;//effectively count number of items
            foreach (var aux in history)
            {
                if (aux.Instrument == id)
                {
                    current += aux.Quantity;
                }
            }

            return current;
        }

        public int getStock(int id)//get numbers of shares for an instrument id

        {
            var history = this._dao.getRecords();
            int nrShares = computeAvailableStock(id, history);
            return nrShares;

        }
        public void computeStock(int id)
        {
            if (id>0)//display numbers os shares for a selected instrument
            {
                this._view.displayStock(getStock(id));

            }
            else
            {
                this._view.showMessage("Invalid selection");
            }
        }
        private  bool modifyMoney(double value)//check if you have enough money for an operation and save them
        {
            if (this.user.Shares + value >= 0)
            {//youi have money
                this.user.Shares += value;
                this._dao.updateUser(user);
                this._view.showMessage("Successful");
                this.displayMoney();
                return true;
            }
            else
            {
                //you dont have money
                this._view.showMessage("Insufficient funds");
                return false;
            }

        }
        public void transaction(int id,double price, int quantity)//check and do a transaction if you have the money or if you have the required stocks
        {
            int stock = getStock(id);//make a new trasnaction
            if (quantity + stock >= 0)
            {

                if (this.modifyMoney(-quantity * price))//check if you have enough money
                {
                    Record deal = new Record
                    {
                        Date = DateTime.Now,
                        Id = -1,
                        Instrument = id,
                        Price = price,
                        Quantity = quantity

                    };
                    this.dailyRecords.Add(deal);//save record
                    this._dao.addRecord(deal);

                }

                

            }
            else
            {
                this._view.showMessage("Insufficient quantity");
            }
            this.displayMoney();
        }

        private void updateRecord(Record record, double price, int quantity)//update a record
        {
            record.Quantity = quantity;//update record in db and in local history
            record.Price = price;
            
            this._dao.updateRecord(record);
        }

        public void updateTransaction(Record transaction, double price, int quantity)

        {
            if (transaction.Quantity != quantity)//check if you changed quantity
            {
                int differentQuantity = transaction.Quantity - quantity;
                double gainedMoney = differentQuantity * transaction.Price;
                if (this.modifyMoney(gainedMoney))
                {
                    updateRecord(transaction,price,quantity);
                   var outdated= this.dailyRecords.FirstOrDefault(x => x.Id == transaction.Id);
                    outdated = transaction;
                }
            }
            else if (transaction.Price != price)//check if price changed
            {
                double differentPrice = transaction.Price - price;
                double gainedMoney = differentPrice * transaction.Quantity;
                if (modifyMoney(gainedMoney))

                {
                    updateRecord(transaction, price, quantity);
                }

            }
            else
            {
                
                this._view.showMessage("Nothing to update");
            }
        }

       /* public String getInstrumentName(int id)
        {
            
            return instrument.Name;
        }
        */
        private void generateXml(List<Record> records)
        {
            var stock = this._dao.getShares();

          
         //ia fiecare record si creeaza un nou tip in care in lod de instrument id-ul 1 pui Sony de ex
            var agregated = records.Select(x =>
            {
                string instrumentName =  stock.FirstOrDefault(y => y.Id == x.Instrument).Name;//ia numele instrumentului ca string
                return new
                {
                    Id = x.Id,
                    Quantity = x.Quantity,
                    Date = x.Date,
                    Price = x.Price,
                     Intrument = instrumentName
                };
            }).ToList();
            
            Factory<object> fact=new Factory<object>();
            //var factoryType = typeof(Factory<>).MakeGenericType(object);
            // dynamic factory = Activator.CreateInstance(factoryType);

            fact.WriteToCsv(agregated);//write report
        }
        public void generateIntervalReport(DateTime start, DateTime stop)
        {
            var records = this._dao.getRecords();//get all records and see which ones are between your interval
            var filtered = records.Where(x =>
                (DateTime.Compare(x.Date, start) >= 0) && (DateTime.Compare(x.Date, stop) <= 0)).ToList();//lambda filter them
            this.generateXml(filtered);
            this._view.showMessage("Successful");

        }

        public void generateTodayReport()
        {
            this.generateXml(dailyRecords);
            this._view.showMessage("Successful");

        }
       public void switchToClassicDB()
        {
            this._dao = new ClassicDao();
            this._view.showMessage("Succes");
        }
        public void switchToHibernate()
        {
            this._dao = new HibernateDao();
            this._view.showMessage("Succes");
        }

        public void deleteRecord(double price, int quantity,Record record)
        {
            if (this.modifyMoney(price * quantity))
            {
                this._dao.deleteRecord(record);
            }
        }

    }



    
}
