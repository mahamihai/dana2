﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using HM2.Models;
using HM2.View;
using HM2.View.Interfaces;


namespace HM2.Controller
{
    public class LoginController:ILoginController
    {
        private ILoginView _view;
        private IDao _dao;
        public LoginController(ILoginView view ,IDao dao)//salveaza variabilele de instanta
        {
           this._view = view;
            
            this._dao = dao;
            this._view.registerController(this);
        }

        public User checkUser(string username, string password)
        {
           var user =this._dao.checkUser(username, password);//verifica daca parola si user is bune
            if (user != null)
            {
                this._view.showMessage("Succesfull loged in");
                IBrokerView view=new BrokerUI();
                IDao dao =new HibernateDao();
                BrokerController controler=new BrokerController(view,dao,user);//porneste o sesiune de broker


            }
            else
            {
                this._view.showMessage("Failed to log in");
            }

            return user;

        }

       

    }
}
