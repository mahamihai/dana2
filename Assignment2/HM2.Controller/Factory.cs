﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM2.Controller
{
    public  class Factory<T> where T : class
    {
        public  void WriteToCsv<T>(List<T> list)//generare repor
        {
            //write to xml using reflection
            var csv = new StringBuilder();

            using (var w = new StreamWriter(@".\test.csv"))
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string comma = "";
                    StringBuilder line = new StringBuilder("");
                    foreach (var f in typeof(T).GetProperties())//loopeaza pe proprietatile clasei si scriele in fisier
                    {
                        line.Append(comma + f.Name);
                        comma = ",";
                        line.Append(comma + f.GetValue(list[i]));


                    }

                    w.WriteLine(line.ToString());
                    w.Flush();

                }

                w.Flush();
                w.Close();
            }
        }
    }
}
