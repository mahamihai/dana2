﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.Controller;
using HM2.Models;
using HM2.View;
using HM2.View.Interfaces;

namespace HM2.App
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            ILoginView loginForm=new Login();
          
            IDao dao=new HibernateDao();
            LoginController controller=new LoginController(loginForm,dao);
            
            loginForm.display();
        }
    }
}
